package com.rommelpalma.testespresso;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{

    EditText edt1, edt2;
    Button main_btn_check;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setViews();

    }

    private void setViews() {
        edt1 = findViewById(R.id.edt1);
        edt2 = findViewById(R.id.edt2);
        main_btn_check.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v == main_btn_check)
        {
            if (validarusuario1(edt1.getText().toString(), edt2.getText().toString()))
            {
                Log.e("test", "paso la prueba");
            }
            else
            {
                Log.e("test", "no paso la validacion");
            }
        }
    }


    public boolean validarusuario1(String email, String password) {

        if (test.camposvacios(email) || test.camposvacios(password)) {
            Toast.makeText(this, "VALIDACION NO VALIDA", Toast.LENGTH_SHORT).show();

            return false;

        }
        if (!test.validaremail(email)) {
            Toast.makeText(this, "VALIDACION DE EMAIL NO VALIDA", Toast.LENGTH_SHORT).show();

            return false;
        }
        if (!test.validarusuario(email, password)) {
            Toast.makeText(this, "VALIDACION DE USUARIO NO CORRECTA", Toast.LENGTH_SHORT).show();

            return false;
        }


        return true;
    }

}