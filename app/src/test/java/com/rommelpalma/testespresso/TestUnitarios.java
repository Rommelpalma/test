package com.rommelpalma.testespresso;

import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestUnitarios {

    @Test
    public void edittext_validar_campo_vacio_verdadero() {


        assertTrue(test.camposvacios(""));
    }

    @Test
    public void edittext_validar_campo_vacio_falso() {


        assertFalse(test.camposvacios("noenvianada"));
    }

    @Test
    public void validar_correo_falso() {

        assertFalse(test.validaremail("rommelpalma.com"));
    }

    @Test
    public void validar_correo_verdadero() {

        assertTrue(test.validaremail("rommel@gmail.com"));
    }

    @Test
    public void validar_usuario_verdadero() {

        assertTrue(test.validarusuario("rommel@gmail.com","222222"));
    }

    @Test
    public void validar_usuario_falso() {

        assertFalse(test.validarusuario("palmarommel@gmail.com","222222"));
    }


}
